---
title: Installer et configurer AWX
subtitle: Comprendre, installer et paramétrer AWX.
layout: articles
modal-id: 1
date: 2019-08-10   
img: AWX.png
thumbnail: AWX.png
alt: AWX Logo
category: automatisation
author: Cyril TAVIAN
---

### Les étapes : 
- Présentation d'AWX.
- Où installer AWX.
- Installation sous Docker. 
- Configuration.
- Test et déploiement.

#### Présentation

AWX permet d'orchestrer l'execution de vos playbooks. 

#### Ou installer AWX

#### Installation sous Docker 

#### Configuration 

#### Test et déploiement




curl -fsSLO https://get.docker.com/builds/Linux/x86_64/docker-17.04.0-ce.tgz \&& tar xzvf docker-17.04.0-ce.tgz \&& mv docker/docker /usr/local/bin \&& rm -r docker docker-17.04.0-ce.tgz