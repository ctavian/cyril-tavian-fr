[![Build Status](https://travis-ci.org/ctavian/cyril-tavian.fr.svg?branch=master)](https://travis-ci.org/ctavian/cyril-tavian.fr)

# Theme cyril-tavian.fr 

## Information 
Ce thème est un fork de [Agency-jekyll-theme](https://github.com/y7kim/agency-jekyll-theme), celui-ci a été modifié afin d'être utilisé en tant que blog et blog.

## Comment l'utiliser ? 
Les articles du blog sont positionnés dans le répertoire "_posts". 

## Démonstration
Voici le résultat du thème : [cyril-tavian.fr](https://cyril-tavian.fr)

## Générer et tester le site web
```
# Installation des Gem. 
bundle install 

# Génération du site
jekyll build

# Tester le code HTML 
bundle exec htmlproofer ./_site
```
